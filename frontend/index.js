const form = document.getElementById("form");
const fruitlist = document.getElementById("fruit-list")
const fruits = []

fetch("http://localhost:8080/login", {
        method: 'POST',
        body: JSON.stringify({username: "Admin", password: "hemligt"}),
        credentials: 'include',
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(res => {
        if(res.ok){
            renderUl();
        }
    })

const handleSubmit = async e => {
    e.preventDefault()
    const fruit = e.target.querySelector("input").value;

    console.log(fruit)
    console.log("Saniterad: ", DOMPurify.sanitize(fruit))

    const csrfRes = await fetch("http://localhost:8080/csrf", {credentials: "include"});
    const token = await csrfRes.json()

    const res = await fetch("http://localhost:8080/api/fruits", {
        credentials: "include",
        method: 'POST',
        body: JSON.stringify({name: fruit}),
        headers: {
            'Content-Type': 'application/json',
            'X-CSRF-TOKEN': token.token
        }
    })

    const data = await res.json();
    console.log(data.name)

    //fruits.push(fruit)

    renderUl()
}

form.addEventListener("submit", handleSubmit)

const renderUl = async () => {

    const res = await fetch("http://localhost:8080/api/fruits", {credentials: "include"});
    const body = await res.json();


    //const fruitHtmlString = body.reduce((acc, curr) => acc + `<li><p>${curr.name}</p></li>`, "")

    // Farlig - vår webbläsare tolkar taggar och exekverar ev. skadlig kod
    // fruitlist.innerHTML = fruitHtmlString;

    fruitlist.textContent = ""

    for(const fruit of body){
        const li = document.createElement("li")
        li.textContent = fruit.name;
        fruitlist.append(li)
    }

}

//renderUl();