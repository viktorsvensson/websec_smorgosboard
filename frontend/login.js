const values = {
    client_id: "fronten",
    response_type: "code",
    scope: "openid",
    redirect_uri: "http://localhost:63342/websecurity_demo/frontend/login.html",
    grant_type: "authorization_code",
    auth_endpoint: "http://localhost:8095/oauth2/authorize",
    token_endpoint: "http://localhost:8095/oauth2/token"
}

document.getElementById("login-btn").addEventListener("click", async () => {

    const code_verifier = window.crypto.getRandomValues(new Uint32Array(20)).join('')
    const code_challenge = await generateCodeChallenge(code_verifier)

    console.log(code_verifier)
    console.log(code_challenge)

    localStorage.setItem("code_verifier", code_verifier)

    const url = values.auth_endpoint
        + "?response_type=" + values.response_type
        + "&client_id=" + values.client_id
        + "&scope=" + values.scope
        + "&redirect_uri=" + values.redirect_uri
        + "&code_challenge=" + code_challenge
        + "&code_challenge_method=" + "S256"

    window.location = url;

})

const searchParams = new URLSearchParams(window.location.search)

if(searchParams.has("code")){
    fetch(values.token_endpoint, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        body: new URLSearchParams({
            "client_id": values.client_id,
            "redirect_uri": values.redirect_uri,
            "grant_type": values.grant_type,
            "code": searchParams.get("code"),
            "code_verifier": localStorage.getItem("code_verifier")
        })
    })
        .then(res => res.json())
        .then(body => {
            document.getElementById("access-token").innerText = body.access_token
            document.getElementById("username").innerText = JSON.parse(atob(body.id_token.split('.')[1])).sub

            fetch("http://localhost:8094/admin", {
                headers: {
                    "Authorization": "Bearer " + body.access_token,
                    "Content-Type": "application/json"
                }
            })
                .then(res => res.text())
                .then(body => console.log(body))
        })
}


const generateCodeChallenge = async code_verifier => {
    const encoder = new TextEncoder();
    const data = encoder.encode(code_verifier)
    const digest = await window.crypto.subtle.digest("SHA-256", data)
    return btoa(String.fromCharCode.apply(null, new Uint8Array(digest))).replace(/\+/g, '-').replace(/\//g, '_').replace(/=+$/, '')
}