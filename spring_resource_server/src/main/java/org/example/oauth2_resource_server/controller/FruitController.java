package org.example.oauth2_resource_server.controller;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/fruit")
public class FruitController {

    public record Fruit(String name){}

    @GetMapping
    public List<Fruit> getFruits(){
        return List.of(new Fruit("Banan"), new Fruit("Apelsin"));
    }
}