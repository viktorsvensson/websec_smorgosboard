package org.example.websecurity_demo;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import org.example.websecurity_demo.entity.AppUser;
import org.example.websecurity_demo.repo.AppUserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Arrays;
import java.util.List;

@SpringBootApplication
public class WebsecurityDemoApplication implements CommandLineRunner {

    @Autowired
    AppUserRepo appUserRepo;

    @Autowired
    PasswordEncoder passwordEncoder;

    @PersistenceContext
    EntityManager entityManager;

    public static void main(String[] args) {
        SpringApplication.run(WebsecurityDemoApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        AppUser admin = new AppUser("Admin", passwordEncoder.encode("hemligt"));
        appUserRepo.save(admin);
    }


        /*

        String username = "' OR 1=1--";
        String password = "hemligt1";

        appUserRepo.findAppUserByUsernameAndPassword(username, password)
                .ifPresentOrElse(System.out::println, () -> System.out.println("Fanns ej"));

        String queryString = "SELECT * FROM app_user WHERE username ='" + username + "' AND password = '" + password + "'";

        System.out.println(
                (AppUser) entityManager
                        .createNativeQuery(queryString, AppUser.class)
                        .getSingleResult()
        );
        System.out.println(queryString);
    }


    @Bean
    public MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter(){
        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        converter.setSupportedMediaTypes(List.of(MediaType.ALL));
        return converter;
    }
*/

}
