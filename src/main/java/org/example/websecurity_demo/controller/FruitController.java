package org.example.websecurity_demo.controller;

import jakarta.validation.Valid;
import jakarta.validation.constraints.AssertTrue;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Positive;
import jakarta.validation.constraints.Size;
import org.example.websecurity_demo.dto.FruitCreationDTO;
import org.example.websecurity_demo.entity.Fruit;
import org.example.websecurity_demo.repo.FruitRepo;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/fruits")
@CrossOrigin(origins = "http://localhost:63342", allowCredentials = "true")
public class FruitController {

    private final FruitRepo fruitRepo;

    public FruitController(FruitRepo fruitRepo) {
        this.fruitRepo = fruitRepo;
    }

    @GetMapping
    public List<Fruit> findAll(Principal principal, @RequestHeader("Origin") Optional<String> origin){
        System.out.println(principal.getName());
        System.out.println(origin.orElse("Same origin - localhost:3000"));
        return fruitRepo.findAll();
    }

    @PostMapping
    public Fruit addFruit(@Valid @RequestBody FruitCreationDTO fruitCreationDTO,
            Principal principal,
            @RequestHeader("Origin") Optional<String> origin
    ){
        System.out.println(principal.getName() + " - post");
        System.out.println(origin.orElse("Same origin - localhost:3000")  + " - post");
        return fruitRepo.save(fruitCreationDTO.toFruit());
    }
}
