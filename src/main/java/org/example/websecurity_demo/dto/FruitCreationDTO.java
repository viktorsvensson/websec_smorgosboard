package org.example.websecurity_demo.dto;

import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import org.example.websecurity_demo.entity.Fruit;

public class FruitCreationDTO {

    @Size(min = 3, max = 20)
    @Pattern(regexp = "^[\\p{L}]*$")
    private String name;

    public FruitCreationDTO(String name) {
        this.name = name;
    }

    public FruitCreationDTO() {
    }

    public Fruit toFruit(){
        return new Fruit(name);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
