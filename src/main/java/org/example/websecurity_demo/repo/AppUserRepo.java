package org.example.websecurity_demo.repo;

import org.example.websecurity_demo.entity.AppUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AppUserRepo extends JpaRepository<AppUser, Integer> {

    @Query(value = "SELECT * FROM app_user WHERE username = ?1 AND password = ?2", nativeQuery = true)
    Optional<AppUser> findAppUserByUsernameAndPassword(String username, String password);

    Optional<AppUser> findAppUsersByUsername(String username);

}
