package org.example.websecurity_demo.repo;

import org.example.websecurity_demo.entity.Fruit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FruitRepo extends JpaRepository<Fruit, Integer> {
}
