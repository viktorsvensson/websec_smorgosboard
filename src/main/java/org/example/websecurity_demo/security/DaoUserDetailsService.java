package org.example.websecurity_demo.security;

import org.example.websecurity_demo.entity.AppUser;
import org.example.websecurity_demo.repo.AppUserRepo;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DaoUserDetailsService implements UserDetailsService {

    private final AppUserRepo appUserRepo;

    public DaoUserDetailsService(AppUserRepo appUserRepo) {
        this.appUserRepo = appUserRepo;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        AppUser appUser = appUserRepo.findAppUsersByUsername(username).orElseThrow();

        return new User(appUser.getUsername(), appUser.getPassword(), List.of(new SimpleGrantedAuthority("USER")));
    }
}
